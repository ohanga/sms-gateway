FROM tutum/lamp:latest
RUN rm -fr /app && git clone https://github.com/ohangahenry/sms-gateway.git /app
WORKDIR /app

EXPOSE 80 3306
CMD ["/run.sh"]